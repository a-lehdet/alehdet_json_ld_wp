<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.a-lehdet.fi
 * @since             1.0.0
 * @package           Alehdet_json_ld
 *
 * @wordpress-plugin
 * Plugin Name:       A-lehdet JSON-LD
 * Plugin URI:        http://www.a-lehdet.fi
 * Description:       Generates JSON-LD data on all the post pages.
 * Version:           1.0.0
 * Author:            A-lehdet Oy
 * Author URI:        http://www.a-lehdet.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       alehdet_json_ld
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-alehdet_json_ld-activator.php
 */
function activate_alehdet_json_ld() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-alehdet_json_ld-activator.php';
	Alehdet_json_ld_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-alehdet_json_ld-deactivator.php
 */
function deactivate_alehdet_json_ld() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-alehdet_json_ld-deactivator.php';
	Alehdet_json_ld_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_alehdet_json_ld' );
register_deactivation_hook( __FILE__, 'deactivate_alehdet_json_ld' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-alehdet_json_ld.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_alehdet_json_ld() {

	$plugin = new Alehdet_json_ld();
	$plugin->run();

}
run_alehdet_json_ld();
