<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.a-lehdet.fi
 * @since      1.0.0
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/admin
 * @author     A-lehdet Oy <digitekniikka@a-lehdet.fi>
 */
class Alehdet_json_ld_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 *  Values used in the fields callbacks
	 *  @var [type]
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Alehdet_json_ld_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alehdet_json_ld_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/alehdet_json_ld-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Alehdet_json_ld_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alehdet_json_ld_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/alehdet_json_ld-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 *  Add options/settings page
	 *  @return [type] [description]
	 */
	public function add_plugin_setup_page(){
		add_menu_page('A-lehdet JSON-LD – Settings', 'JSON-LD', 'manage_options', 'alehdet-json-ld-settings', array( $this, 'create_settings_page') );
	}

	/**
	 *  Menu page callback
	 *  @return [type] [description]
	 */
	public function create_settings_page() {
		// Set class property
    $this->options = get_option( 'alehdet-json-ld-options' );
    ?>
		<div class="wrap">
			<h1>A-lehdet JSON-LD – Settings</h1>
			<form method="post" action="options.php">
				<?php settings_fields( 'alehdet-json-ld-group' ); ?>
				<?php do_settings_sections( 'alehdet-json-ld-settings' ); ?>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}

	/**
	 *  Register and add settings and fields
	 *  @return [type] [description]
	 */
	public function page_init() {
		register_setting(
      'alehdet-json-ld-group', 		// Option group
      'alehdet-json-ld-options',	// Option name
      array( $this, 'sanitize' ) 	// Sanitize
    );

		add_settings_section(
			'alehdet-json-ld-general', 						// ID
			'General settings', 									// Title
			array( $this, 'print_section_info' ), // Callback
			'alehdet-json-ld-settings' 						// Page
		);

		add_settings_field(
			'type', 															 // ID
			'Type', 			 												 // Title
			array( $this, 'type_field_callback' ), // Callback
			'alehdet-json-ld-settings', 					 // Page
			'alehdet-json-ld-general' 						 // Section
		);

		add_settings_field(
			'organization',
			'Organization',
			array( $this, 'organization_field_callback' ),
			'alehdet-json-ld-settings',
			'alehdet-json-ld-general'
		);

		add_settings_field(
			'default-level',
			'Default level',
			array( $this, 'default_level_field_callback' ),
			'alehdet-json-ld-settings',
			'alehdet-json-ld-general'
		);
	}

	/**
	 *  Sanitize each setting field as needed
	 *  @param  array $input Contains all settings fields as array keys
	 *  @return [type]        [description]
	 */
  public function sanitize( $input ) {
    $new_input = array();

    if( isset( $input['type'] ) )
      $new_input['type'] = sanitize_text_field( $input['type'] );

		if( isset( $input['organization'] ) )
      $new_input['organization'] = sanitize_text_field( $input['organization'] );

		if( isset( $input['default-level'] ) )
			$new_input['default-level'] = intval($input['default-level']);

    return $new_input;
  }

	/**
	 *  Print the Section text
	 *  @return [type] [description]
	 */
	public function print_section_info() {
  	_e('Set some values that will be used on every post when JSON-LD data is generated. To find out more about this go read the <a href="https://a-lehdet.atlassian.net/wiki/display/AAT/JSON-LD" target="_blank">documentation</a>.');
  }

	/**
   * Callback function for "Type"-field
   */
  public function type_field_callback() {
    printf(
    	'<input type="text" id="type" name="alehdet-json-ld-options[type]" value="%s" />
			<p><small>' . __('Used as JSON-LD data\'s <code>@type</code> field\'s value. If left empty <em>"WebPage"</em> is used.') . '</small></p>',
      isset( $this->options['type'] ) ? esc_attr( $this->options['type']) : ''
    );
  }

  /**
   * Callback function for "Organization"-field
   */
  public function organization_field_callback() {
    printf(
    	'<input type="text" id="organization" name="alehdet-json-ld-options[organization]" value="%s" />
			<p><small>' . __('Used as JSON-LD data\'s <code>@publisher</code> field\'s "name" field\'s value. If left empty <em>"A-lehdet Oy"</em> is used.') . '</small></p>',
      isset( $this->options['organization'] ) ? esc_attr( $this->options['organization']) : ''
    );
  }

	/**
   * * Callback function for "Default level"-field
   */
  public function default_level_field_callback() {
		$default_level_option = is_numeric($this->options['default-level'])?$this->options['default-level']:1;
		?>
		<select id="default-level" name="alehdet-json-ld-options[default-level]">
		  <option value="0"<?php echo $default_level_option == 0?" selected=\"selected\"":"" ?>>0</option>
		  <option value="1"<?php echo $default_level_option == 1?" selected=\"selected\"":"" ?>>1</option>
		  <option value="2"<?php echo $default_level_option == 2?" selected=\"selected\"":"" ?>>2</option>
		</select>
		<p><small><?php _e('How much JSON-LD data\'s is generated if <code>json-ld</code> query parameter is not present. 0 = no JSON-LD data at all, 1 = minimum ammount of data, 2 = extended fieldset. Find out more about the levels in the <a href="https://a-lehdet.atlassian.net/wiki/display/AAT/JSON-LD" target="_blank">documentation</a>.'); ?></small></p>
    <?php
  }

}
