<?php

/**
 * Constant variables.
 */
// Set default values that will be used as backup values on the JSON-LD fields
// if these haven't been set in the plugin's settings page
define('ALEHDET_JSON_LD_DEFAULT_TYPE', 'WebPage');
define('ALEHDET_JSON_LD_DEFAULT_ORGANIZATION', 'A-lehdet Oy');
define('ALEHDET_JSON_LD_DEFAULT_LEVEL', 1);


/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.a-lehdet.fi
 * @since      1.0.0
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/public
 * @author     A-lehdet Oy <digitekniikka@a-lehdet.fi>
 */
class Alehdet_json_ld_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->plugin_settings = get_option( 'alehdet-json-ld-options' );

		/**
		 * The class responsible for providing helper functions.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-alehdet_json_ld-helper.php';

		$this->helper = new Alehdet_json_ld_Helper();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Alehdet_json_ld_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alehdet_json_ld_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/alehdet_json_ld-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Alehdet_json_ld_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alehdet_json_ld_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/alehdet_json_ld-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 *  Define a custom query variable/parameter to get
	 *  the JSON-LD level
	 *
	 *  @param [type] $vars [description]
	 */
	public function add_query_vars_filter( $vars ){
		$vars[] = "json-ld";
		return $vars;
	}

	/**
	 *  Add JSON-LD data to the post pages
	 *
	 *  @since 1.0.0
	 */
	public function add_json_ld_to_head() {
		global $post;

		// Check that the post was not empty and its type is "post"
		if(!empty($post) && $post->post_type == "post") {

			// Get the JSON-LD query parameters value to a variable
			$json_ld_param 				 = get_query_var( 'json-ld' );
			$json_ld_level         = is_numeric($this->plugin_settings['default-level'])?$this->plugin_settings['default-level']:ALEHDET_JSON_LD_DEFAULT_LEVEL;

			// Set default value for that the JSON-LD data should be created
      $generate_json_ld_data = true;
      // Set default value for indicating is the node a commercial node
      $commercial            = false;
      // Init an empty array for content tags
      $contentTags           = array();

			// Check if the query parameter was provided and it was an number
			// set its value as the level
			if(isset($json_ld_param) && is_numeric($json_ld_param)) {
        $json_ld_level = intval($json_ld_param);
      }

			// If the query parameter was 0 or the default level was set on the
			// plugin's settings page, then don't create any JSON-LD data
			if($json_ld_level == 0) $generate_json_ld_data = false;

			// Generate an array which will include all the fields for JSON-LD
			// and that will be givan as a parameter for json_encode-function
			if($generate_json_ld_data):

				$keywords = $this->helper->alehdet_json_ld_get_post_keywords($post->ID);

				$json_ld_params = array(
					'@context'      => array(
						"http://schema.org",
						array(
							"campaignCode" => "http://www.a-lehdet.fi/context#campaignCode",
							"commercial"   => "http://www.a-lehdet.fi/context#commercial",
							"contentTag"   => "http://www.a-lehdet.fi/context#contentTag",
							"contentType"  => "http://www.a-lehdet.fi/context#contentType"
						)),
					'@type'         => !empty($this->plugin_settings["type"])?$this->plugin_settings["type"]:ALEHDET_JSON_LD_DEFAULT_TYPE,
					'url'           => get_permalink($post->ID),
					'headline'      => $post->post_title,
					'keywords'      => $keywords,
					'author'        => array(
						"@type" => "Person",
						"name"  => get_the_author()
					),
					'publisher'     => array(
						"@type" => "Organization",
						"name"  => !empty($this->plugin_settings["organization"])?$this->plugin_settings["organization"]:ALEHDET_JSON_LD_DEFAULT_ORGANIZATION,
					),
					'dateCreated'   => get_the_date( "c", $post->ID ),
					'datePublished' => get_the_date( "c", $post->ID ),
					'dateModified'  => get_the_modified_date("c")
				);

				// Check if the extended version of JSON-LD data should
        // be generated.
        if ($json_ld_level == 2):

					// Content type to be used in JSON-LD data's contentType-field
					// TODO: This should be made dynamic ASAP, because post is used
					// at least as an article and a blog post on different WP-sites.
          $schema_content_type = $this->helper->alehdet_json_ld_get_schema_content_type($post->post_type);

					// Add new fields to the JSON-LD array
          $json_ld_params += array(
            'text'         => array(wp_strip_all_tags($post->post_content)),
            'comment'      => array(),	// This has not been implemented on any WP-sites uniformly, so a hardcoded value will be used.
            'image'        => array(wp_get_attachment_url(get_post_thumbnail_id($post->ID))),
            'video'        => array(),	// This has not been implemented on any WP-sites uniformly, so a hardcoded value will be used.
            'contentType'  => $schema_content_type,
            'contentTag'   => array(), 	// This has not been implemented on any WP-sites uniformly, so a hardcoded value will be used.
            'commercial'   => false,		// This has not been implemented on any WP-sites uniformly, so a hardcoded value will be used.
            'campaignCode' => "" 				// This has not been implemented on any WP-sites uniformly, so a hardcoded value will be used.
          );

				endif; // ENDIF: $json_ld_level == 2

				?>
				<script type="application/ld+json">
					<?php echo json_encode($json_ld_params, JSON_UNESCAPED_SLASHES); ?>
				</script>
				<?php

			endif; // ENDIF: $generate_json_ld_data

	  }
 	}

}
