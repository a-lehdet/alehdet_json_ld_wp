<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.a-lehdet.fi
 * @since      1.0.0
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 * @author     A-lehdet Oy <digitekniikka@a-lehdet.fi>
 */
class Alehdet_json_ld_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'alehdet_json_ld',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
