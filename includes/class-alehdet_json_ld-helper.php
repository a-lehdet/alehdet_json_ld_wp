<?php

/**
 * Helper functions
 *
 * @link       http://www.a-lehdet.fi
 * @since      1.0.0
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 */

/**
 * Helper functions
 *
 * This class defines all code for helper/helpful functions.
 *
 * @since      1.0.0
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 * @author     A-lehdet Oy <digitekniikka@a-lehdet.fi>
 */
class Alehdet_json_ld_Helper {

	 /**
		*  Generates value for JSON-LD data's "contentType"-field
		*  based on the post's type
		*
		*  @param  String $type Post's type from the post-object (machine name)
		*  @return String       If the type has a match, then the JSON-LD data's content type will be returned. Otherwise null.
		*  @since  1.0.0
		*/
	public static function alehdet_json_ld_get_schema_content_type($type = null) {
    $generated_content_type = "";

    if($type) {

      if(preg_match("/article/i", $type)) {
        $generated_content_type = "article";
      } else if (preg_match("/post/i", $type)) {
        $generated_content_type = "blog_post";
      }
    }

    return $generated_content_type;
  }

	/**
	 *  Generate string for JSON-LD keywords using the categories
	 *  that the post has
	 *
	 *  @param  int $post_id Post's ID
	 *  @return String       Comma separated list of categories that the post has. If post has none, then value will be an empty string.
	 *  @since 	1.0.0
	 */
	public static function alehdet_json_ld_get_post_keywords($post_id = null) {
		$keywords = "";

		if($post_id) {

			$post_categories = wp_get_post_categories( $post_id );
			$categories      = array();
			$category_count  = count($post_categories);
			$i               = 0;

			foreach ($post_categories as $key => $value) {
				$category_data = get_category( $value );
				$keywords .= ($category_count-1 == $i) ? $category_data->name : $category_data->name . ",";
		    $i++;
		  }

		}

		return $keywords;
	}

}
