<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.a-lehdet.fi
 * @since      1.0.0
 *
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Alehdet_json_ld
 * @subpackage Alehdet_json_ld/includes
 * @author     A-lehdet Oy <digitekniikka@a-lehdet.fi>
 */
class Alehdet_json_ld_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
