# JSON-LD plugin for WordPress

*updated 11.1.2017*

This plugin generates JSON-LD data on all the post pages. The JSON-LD data's structure and detailed specifications are at [Confluence's Wiki](https://a-lehdet.atlassian.net/wiki/display/AAT/JSON-LD).

By default a basic set (i.e. level 1) of JSON-LD data will be generated on every post page. The amount of generated data can be controlled by giving a query parameter `json-ld`. The parameter's handling code checks that it's an integer.

Parameter's value | Info
------------------|--------------------------
0 (`json-ld=0`)   | Disables JSON-LD data's generation entirely i.e. there won't be anything added inside the head-tag.
1 (`json-ld=1`)   | Generates the basic set (level 1) of JSON-LD data.
2 (`json-ld=2`)   | Complete JSON-LD data set that addition to level 1 data has some extra fields for analytics purposes.

## Installation

Add the plugin to the sites `plugins`-folder and activate it from the site's plugins list. After the activation some settings should be set from the plugin's settings page (`JSON-LD` menu item on the admin panel's left side). If these are not set, then default values are used. More about the default values can be found in the settings page. After the activation and possibly modifying settings the plugin should work and automatically generate JSON-LD data to post pages.

## What data from posts are used?

In the first version of the plugin some data is hard coded. This is because majority of WP sites implementations for similar content differs from one another. E.g. video in a post might be entered via own dedicated field (custom or provided by a plugin) or among the body-field. And the format that a video is entered might by an URL, video's ID, or an embed code from various video platforms. This is the situation with the current WP sites, and is a valid issue to keep in mind when the new website platform is designed and defined.

This is why some of the data is hard coded or some default field names are set, that are used in attempt to get data from the site. One way to improve this and make the JSON-LD data generation more suitable for the site's structure/implementation, is to have a field mapper in the plugins's settings page. E.g. site's every post/page type could be listed on the settings page and on each one you could select what post/page type's field should be used on JSON-LD as values. For JSON-LD's `headline` field you could choose to use the post's `title` field or some other field that the content type has.

Underneath is a table which contains what WordPress data's is used or tried to be used on version 1.0.

**Level 1 data**

JSON-LD field   | WordPress field/info
----------------|--------------------------
`@context`      | Hard coded value that will stay the same from site to site. Or if it will be changed, it should be changed to every site that uses this module.
`@type`         | Value is fetched from the settings page. If its not found from the settings page then the default value will be used ("WebPage"). This can be more dynamic in future versions. In that case this might need a field for each content type listed on the module's settings page.
`url`           | Uses WordPress' [get_permalink()-function](https://developer.wordpress.org/reference/functions/get_permalink) to generate absolute URL to the post.
`headline`      | Post's title.
`keywords`      | Uses an helper function `alehdet_json_ld_get_post_keywords` (located in `includes/class-alehdet_json_ld-helper.php`) to get all the categories attached to the post.
`author`        | Uses WordPress' [get_the_author()-function](https://codex.wordpress.org/Function_Reference/get_the_author) to get the authors name.
`publisher`     | Value is fetched from the settings page. If its not found from the settings page then the default value will be used ("A-lehdet Oy").
`dateCreated`   | Uses WordPress' [get_the_date()-function](https://codex.wordpress.org/Function_Reference/get_the_date) to get the post's created date.
`datePublished` | Uses WordPress' [get_the_date()-function](https://codex.wordpress.org/Function_Reference/get_the_date) to get the post's created date.
`dateModified`  | Uses WordPress' [get_the_modified_date()-function](https://codex.wordpress.org/Function_Reference/get_the_modified_date) to get the post's modified date.

**Level 2 data**

JSON-LD field   | WordPress field/info
----------------|--------------------------
`text`          | Post's body run through WordPress' [wp_strip_all_tags()-function](https://codex.wordpress.org/Function_Reference/wp_strip_all_tags).
`comment`       | Hard coded to an empty array. Some sites uses Facebook commenting, hence there is no common and uniform way how comments are implemented across sites. So for that reason in version 1.0 the comments are not included in the data, it will always be an empty array.
`image`         | Post's Featured image -field using [wp_get_attachment_url()-](https://codex.wordpress.org/Function_Reference/wp_get_attachment_url) and [get_post_thumbnail_id()-functions](https://codex.wordpress.org/Function_Reference/get_post_thumbnail_id) to get an URL to the image.
`video`         | Hard coded to an empty array. More info about this can be found it the paragraphs above.
`contentType`   | Uses custom helper function called `alehdet_json_ld_get_schema_content_type()` (located in `includes/class-alehdet_json_ld-helper.php`) that attempts to generate standardized version of post types that can be better used in analytical sense. For now the function reads the site's post type's machine name and with regex searches for specific words from them. If type's name contains the string "article", then the function returns "article" as a type. And if the name contains "post", then "blog_post" is returned. Otherwise an empty string is returned. The values that are needed in analytics should be listed and specified. And then the function should be updated in future versions to implement those specifications.
`contentTag`    | Not implemented on any WordPress site, so at the moment is always an empty array (hard coded).
`commercial`    | Not implemented on any WordPress site, so at the moment is always `false` (hard coded).
`campaignCode`  | Not implemented on any WordPress site, so at the moment is always an empty string (hard coded).
